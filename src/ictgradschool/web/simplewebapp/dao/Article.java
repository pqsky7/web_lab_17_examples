package ictgradschool.web.simplewebapp.dao;

/**
 * Represents an article, which has a title and a body of text.
 *
 */
public class Article {

	private int id;
	private String title;
	private String body;

	/**
	 * Creates a new {@link Article}.
	 * 
	 * @param id
	 *            the article's id
	 * @param title
	 *            the article's title
	 * @param body
	 *            the article's body
	 */
	Article(int id, String title, String body) {
		this.id = id;
		this.title = title;
		this.body = body;
	}

	/**
	 * Gets the article's ID
	 * 
	 * @return
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * Gets the article's title
	 * 
	 * @return
	 */
	public String getTitle() {
		return this.title;
	}

	/**
	 * Gets the article's body
	 * 
	 * @return
	 */
	public String getBody() {
		return this.body;
	}

}