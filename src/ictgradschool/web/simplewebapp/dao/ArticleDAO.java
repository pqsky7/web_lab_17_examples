package ictgradschool.web.simplewebapp.dao;

import ictgradschool.web.simplewebapp.db.AbstractDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Allows us to get articles from elsewhere in the code without worrying about SQL statements.
 */
public class ArticleDAO {

    /**
     * Gets all {@link Article}s from the given {@link AbstractDB}.
     * @param db
     * @return
     */
    public static List<Article> getAllArticles(AbstractDB db) {

        List<Article> articles = new ArrayList<>();

        try (Connection c = db.connection()) {
            try (PreparedStatement p = c.prepareStatement("SELECT * FROM simplewebapp_article")) {
                try (ResultSet r = p.executeQuery()) {
                    while (r.next()) {
                        articles.add(articleFromResultSet(r));
                    }
                }
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return articles;

    }

    /**
     * Gets the {@link Article} with the given id from the given {@link AbstractDB}.
     * @param db
     * @param id
     * @return
     */
    public static Article getArticle(AbstractDB db, int id) {
        Article article = null;

        try (Connection c = db.connection()) {
            try (PreparedStatement p = c.prepareStatement("SELECT * FROM simplewebapp_article WHERE article_id = ?")) {
                p.setInt(1, id);

                try (ResultSet r = p.executeQuery()) {
                    while (r.next()) {
                        article = articleFromResultSet(r);
                    }
                }
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return article;
    }

    /**
     * Extract a {@link Article} object from a given {@link ResultSet}
     *
     * @param r The {@link ResultSet} to extract a {@link Article} from
     * @return A valid {@link Article} object, representing a row from the {@link ResultSet}
     * @throws SQLException Generated in the case of an out-of-bounds column index, or for an invalid {@link ResultSet}
     */
    private static Article articleFromResultSet(ResultSet r) throws SQLException {
        return new Article(
                r.getInt("article_id"),
                r.getString("title"),
                r.getString("body"));
    }

}
