<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="ictgradschool.web.simplewebapp.db.MySQL" %>
<%@ page import="java.util.List" %>
<%@ page import="ictgradschool.web.simplewebapp.dao.Article" %>
<%@ page import="ictgradschool.web.simplewebapp.dao.ArticleDAO" %>
<!DOCTYPE html>
<html>
<head>
    <title>Simple Articles JSP</title>
</head>
<body>

<%

    MySQL db = new MySQL();

    // A scriptlet - we can stick any Java code in here.
    if (request.getParameter("article") == null) {

        List<Article> articles = ArticleDAO.getAllArticles(db);%>

        <section id="view" class="container">

            <%for (Article article : articles) {%>

                <section class="article">

                    <p>
                       <a href="?article=<%=article.getId()%>"><%=article.getTitle()%></a>
                    </p>

                </section>

            <%}%>

        </section>

    <%} else {

        Article article = ArticleDAO.getArticle(db, Integer.parseInt(request.getParameter("article")));%>

        <section id="view" class="container">

            <section class ="article">

                <h1><%=article.getTitle()%></h1>
                <p><%=article.getBody()%></p>


            </section>

        </section>

    <%}%>

</body>
</html>
